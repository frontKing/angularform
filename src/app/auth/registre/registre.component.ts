import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UsersServiceService} from "../../shared/services/users-service.service";
import {UserModel} from "../../shared/models/user.model";
import {ActivatedRoute, Router, Routes} from "@angular/router";
import {Message} from "@angular/compiler/src/i18n/i18n_ast";
import {AuthService} from "../../shared/services/auth.service";

@Component({
  selector: 'app-registre',
  templateUrl: './registre.component.html',
  styleUrls: ['./registre.component.scss']
})
export class RegistreComponent implements OnInit {

  form: FormGroup;
  message: Message;

  constructor(
    private userService: UsersServiceService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      'email': new FormControl(null, [Validators.email, Validators.required], this.forbiddenEmails.bind(this)),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)]),
      'name': new FormControl(null, [Validators.required]),
      'skills': new FormControl(null, [Validators.required]),
    });
  }

  onSubmit() {

    const {email, password, name, skills} = this.form.value;

    const user = new UserModel(email, password, name, skills);

    this.userService.createUser(user)
      .subscribe(() => {
        this.router.navigate(['/login'], {
          queryParams: {
            nowCanLogin: true
          }
        });

      });
  }

  forbiddenEmails(control: FormControl): Promise<any> {

    return new Promise((resolve, reject) => {
      this.userService.getUserByEmail(control.value)
        .subscribe((user: UserModel) => {
          console.log('user', user);

          if (user[0]) {
            /*по правилу асинхронных валидаторов надо передать обьект с ключем и значением*/
            resolve({forbiddenEmail: true});
          } else {

            resolve(null);
          }
        });
    });
  }

}
