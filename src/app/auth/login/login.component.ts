import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UsersServiceService} from "../../shared/services/users-service.service";
import {UserModel} from "../../shared/models/user.model";
import {MessageModel} from "../../shared/models/message.model";
import {AuthService} from "../../shared/services/auth.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Message} from "@angular/compiler/src/i18n/i18n_ast";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  message: MessageModel;

  constructor(
    private usersService: UsersServiceService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.message = new MessageModel('danger', '');

    this.route.queryParams
      .subscribe((params: Params) => {
        if (params['nowCanLogin']) {
          this.showMessage({
            text: 'Теперь вы может зайти в систему',
            type: 'success'
          });
        } else if (params['accessDenied']) {
          this.showMessage({
            text: 'Вам нужно залогиниться',
            type: 'danger'
          })
        }
      });

    this.form = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)])
    });
  }

  showMessage(message: MessageModel) {
    this.message = message;
    window.setTimeout(() => {
      this.message.text = '';
    }, 5000);
  }

  onSubmit() {
    const formData = this.form.value;

    this.usersService.getUserByEmail(formData.email)
      .subscribe((user: UserModel) => {
        if (user[0]) {
          if (user[0].password === formData.password) {
            // this.message.text = '';
            this.showMessage({text: 'Вы зарегистрированы', type: 'success'});
            localStorage.setItem('user', JSON.stringify(user));
            this.authService.login();
            this.router.navigate(['/system', 'all-users']);
          } else {
            this.showMessage({text: 'Пароль не верный', type: 'danger'});
          }
        } else {
          this.showMessage({text: 'Такого пользователя нет', type: 'danger'});
        }
      })
  }

}
