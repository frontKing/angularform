import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserModel} from '../models/user.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersServiceService {

  constructor(
    private http: HttpClient
  ) { }

  getUserByEmail(email: string): Observable<UserModel> {
    return this.http.get<UserModel>(`http://localhost:3000/users?email=${email}`);
  }

  createUser(user: UserModel): Observable<UserModel> {
    return this.http.post<UserModel>(`http://localhost:3000/users`, user);
  }

  getAllUsers(): Observable<UserModel> {
    return this.http.get<UserModel>(`http://localhost:3000/users`).pipe(map( result => {
      return result;
    }));
  }
}
