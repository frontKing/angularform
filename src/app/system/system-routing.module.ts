import {NgModule} from "@angular/core";
import {Router, RouterModule, Routes} from "@angular/router";
import {SystemComponent} from "./system.component";
import {AllUsersComponent} from "./all-users/all-users.component";
import {AuthGuard} from "../shared/services/auth.guard";

const routes: Routes = [
  {path: 'system', component: SystemComponent, canActivate: [AuthGuard], children: [
      {path: 'all-users', component: AllUsersComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SystemRoutingModule {}
