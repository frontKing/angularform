import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {SystemRoutingModule} from "./system-routing.module";
import { AllUsersComponent } from './all-users/all-users.component';
import {SystemComponent} from "./system.component";
import { HeaderComponent } from './shared/components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SystemRoutingModule
  ],
  declarations: [AllUsersComponent, SystemComponent, HeaderComponent]
})

export class SystemModule {}
