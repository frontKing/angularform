import { Component, OnInit } from '@angular/core';
import {UsersServiceService} from "../../shared/services/users-service.service";
import {UserModel} from "../../shared/models/user.model";

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss']
})
export class AllUsersComponent implements OnInit {
  users: UserModel;
  constructor(
    private userService: UsersServiceService
  ) { }

  ngOnInit() {
    this.userService.getAllUsers()
      .subscribe((response) => {
        console.log('response', response);
        this.users = response
      });
  }

}
